package com.chordy.generator

import com.chordy.domain.Measure
import com.chordy.domain.scale.Scale

object MeasureGenerator {
    fun generate(scale: Scale): Measure {
        val notes = (1..4).map { NoteGenerator.generate(scale) }
        return Measure(notes)
    }
}
