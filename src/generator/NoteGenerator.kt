package com.chordy.generator

import com.chordy.domain.Note
import com.chordy.domain.scale.Scale

object NoteGenerator {
    fun generate(scale: Scale): Note {
        return scale.notes.random()
    }
}
