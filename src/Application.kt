package com.chordy

import com.chordy.domain.Composition
import com.chordy.domain.TimeSignature
import com.chordy.domain.scale.Simple
import com.chordy.generator.MeasureGenerator
import com.chordy.musicxml.CompositionSerializer
import domain.KeySignature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.html.respondHtml
import io.ktor.http.ContentType
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import kotlinx.html.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CORS) {
        anyHost()
    }
    routing {
        get("/generate.xml") {
            val composition = Composition(
                measures = (1..10).map { MeasureGenerator.generate(Simple) },
                timeSignature = TimeSignature(4, 4),
                keySignature = KeySignature.CMajor
            )
            val compositionSerializer = CompositionSerializer()
            call.respondText(ContentType.Application.Xml) {
                compositionSerializer.serialize(composition)
            }
        }
        get("/") {
            call.respondHtml {
                head {
                    title {
                        +"Sight-reading exercise"
                    }
                    script(src = "https://unpkg.com/opensheetmusicdisplay") {}
                }
                body {
                    div {
                        id = "div-container"
                        style = "width: 100%"
                    }
                    script {
                        +"var osmd = new opensheetmusicdisplay.OpenSheetMusicDisplay('div-container');\n"
                        +"osmd.load('/generate.xml').then(function () {\n"
                        +"        osmd.render();\n"
                        +"    });"
                    }
                }
            }
        }
    }
}
