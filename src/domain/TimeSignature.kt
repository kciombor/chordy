package com.chordy.domain

data class TimeSignature(
    val beats: Int,
    val beatType: Int
)
