package com.chordy.domain

data class Measure(
    val notes: Collection<Note>
)
