package com.chordy.domain

data class Note(val step: String, val octave: Int)
