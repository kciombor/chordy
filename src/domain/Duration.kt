package com.chordy.domain

enum class Duration(val divisor: Int) {
    WHOLE(1),
    HALF(2),
    QUARTER(4),
    EIGHT(8),
    SIXTEEN(16)
}
