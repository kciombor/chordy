package domain

sealed class KeySignature(
    val fifths: Int
) {
    object CMajor : KeySignature(0)
    object AMinor : KeySignature(0)

    object GMajor : KeySignature(1)
    object EMinor : KeySignature(1)

    object DMajor : KeySignature(2)
    object BMinor : KeySignature(2)

    object AMajor : KeySignature(3)
    object FSharpMinor : KeySignature(3)

    object EMajor : KeySignature(4)
    object CSharpMinor : KeySignature(4)

    object BMajor : KeySignature(5)
    object GSharpMinor : KeySignature(5)

    object FSharpMajor : KeySignature(6)
    object DSharpMinor : KeySignature(6)

    object CSharpMajor : KeySignature(7)

    object CFlatMajor : KeySignature(-7)

    object GFlatMajor : KeySignature(-6)
    object EFlatMinor : KeySignature(-6)

    object DFlatMajor : KeySignature(-5)
    object BFlatMinor : KeySignature(-5)

    object AFlatMajor : KeySignature(-5)
    object FMinor : KeySignature(-5)

    object EFlatMajor : KeySignature(-4)
    object CMinor : KeySignature(-4)

    object BFlatMajor : KeySignature(-2)
    object GMinor : KeySignature(-2)

    object FMajor : KeySignature(-1)
    object DMinor : KeySignature(-1)
}
