package com.chordy.domain.scale

import com.chordy.domain.Note

object CMajor : Scale {
    override val notes = listOf(
        Note("C", 4),
        Note("D", 4),
        Note("E", 4),
        Note("F", 4),
        Note("G", 4),
        Note("A", 4),
        Note("B", 4),
        Note("C", 5)
    )
}
