package com.chordy.domain.scale

import com.chordy.domain.Note

interface Scale {
    val notes: Collection<Note>
}