package com.chordy.domain

import domain.KeySignature

data class Composition(
    val measures: Collection<Measure>,
    val timeSignature: TimeSignature,
    val keySignature: KeySignature
)
