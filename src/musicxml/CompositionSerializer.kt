package com.chordy.musicxml

import com.chordy.domain.Composition
import org.redundent.kotlin.xml.xml

class CompositionSerializer {
    fun serialize(composition: Composition): String {
        return StringBuilder().append(
            Constants.HEADER
        ).append(
            Constants.PARTWISE_HEADER
        ).append(
            serializeScorePartwise(composition)
        ).toString()
    }

    private fun serializeScorePartwise(composition: Composition): String {
        return xml("score-partwise") {
            attribute("version", "3.1")
            "work" {
                "work-title" {
                    -"Sight-reading exercise"
                }
            }
            "part-list" {
                "score-part" {
                    attribute("id", "P1")
                    "part-name" {
                        -"Music"
                    }
                }
            }
            "part" {
                attribute("id", "P1")
                "measure" {
                    attribute("number", 0)
                    attribute("implicit", "yes")
                    "attributes" {
                        "divisions" {
                            -"1"
                        }
                        "key" {
                            "fifths" {
                                -"${composition.keySignature.fifths}"
                            }
                        }
                        "time" {
                            "beats" {
                                -"${composition.timeSignature.beats}"
                            }
                            "beat-type" {
                                -"${composition.timeSignature.beatType}"
                            }
                        }
                        "clef" {
                            "sign" {
                                -"G"
                            }
                            "line" {
                                -"2"
                            }
                        }
                    }
                    for (note in composition.measures.first().notes) {
                        "note" {
                            "pitch" {
                                "step" {
                                    -note.step
                                }
                                "octave" {
                                    -"${note.octave}"
                                }
                            }
                            "duration" {
                                -"1"
                            }
                            "type" {
                                -"quarter"
                            }
                        }
                    }
                }
                for ((index, measure) in composition.measures.drop(1).withIndex()) {
                    "measure" {
                        attribute("number", index + 1)
                        for (note in measure.notes) {
                            "note" {
                                "pitch" {
                                    "step" {
                                        -note.step
                                    }
                                    "octave" {
                                        -"${note.octave}"
                                    }
                                }
                                "duration" {
                                    -"1"
                                }
                                "type" {
                                    -"quarter"
                                }
                            }
                        }
                    }
                }
            }
        }.toString(false)
    }
}
