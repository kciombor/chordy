package com.chordy

import com.chordy.domain.Composition
import domain.KeySignature
import com.chordy.domain.Measure
import com.chordy.domain.TimeSignature
import com.chordy.domain.scale.Simple
import com.chordy.generator.NoteGenerator
import com.chordy.musicxml.CompositionSerializer

fun main() {
    val notes = (1..20).map { NoteGenerator.generate(Simple) }
    val composition = Composition(
        measures = listOf(Measure(notes)),
        timeSignature = TimeSignature(4, 4),
        keySignature = KeySignature.CMajor
    )
    val compositionSerializer = CompositionSerializer()
    println(compositionSerializer.serialize(composition))
}
