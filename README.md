# Chordy
This is a simple for generating random sight-reading exercises.

The app is written in [Kotlin](https://kotlinlang.org/) using [Ktor](https://ktor.io/) framework and uses [OpenSheetMusic](https://opensheetmusicdisplay.org/) for rendering sheet music.

The app is publicly available at https://chordy-be.herokuapp.com/
